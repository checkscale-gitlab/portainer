# Portainer


Portainer is a simple management UI for Docker.
https://portainer.io/



<!-- TOC -->
- [1.  Configuration](#1--configuration)
    - [1.1. Environments](#11-environments)
    - [1.2. Endpoint](#12-endpoint)
- [2. Templates](#2-templates)
<!-- /TOC -->

## 1.  Configuration
---

### 1.1. Environments

```
#!/usr/bin/env
# environment variables for Docker-Compose
# Source: https://docs.docker.com/compose/environment-variables/#the-env-file
#
TZ=Europe/Zurich
STACK_HOST=portainer.example.com
TRAEFIK_MIDDLEWARE=your-auth-middleware
```

### 1.2. Endpoint

Skip the first setup and add manually a new docker endpoint.

The Endpoint URL is `dsproxy:2375`

> Do not connect via socket, keep this option and also keep TLS unchecked.

## 2. Templates

* https://raw.githubusercontent.com/dnburgess/self-hosted-template/master/template.json
* https://raw.githubusercontent.com/Qballjos/portainer_templates/master/Template/template.json
* https://raw.githubusercontent.com/SelfhostedPro/selfhosted_templates/portainer-2.0/Template/template.json
* https://raw.githubusercontent.com/technorabilia/portainer-templates/main/lsio/templates/templates-2.0.json
* https://raw.githubusercontent.com/mikestraney/portainer-templates/master/templates.json
